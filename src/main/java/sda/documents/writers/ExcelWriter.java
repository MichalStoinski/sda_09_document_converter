package sda.documents.writers;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import sda.documents.exceptions.FileWriterException;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class ExcelWriter extends IFileWriter {

    private static final String SHEET_NAME = "data";

    @Override
    public void write(String filePath, List<Map<String, String>> data) throws FileWriterException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet(SHEET_NAME);
        List<String> headers = prepareHeaderRow(data);

        int rowNumber = 0;
        int colNumber = 0;
        Row headerRow = sheet.createRow(rowNumber++);
        for (String header : headers) {
            Cell cell = headerRow.createCell(colNumber++);
            cell.setCellValue(header);
        }

        for (Map<String, String> rowData : data) {
            Row row = sheet.createRow(rowNumber++);
            colNumber = 0;
            for (String header : headers) {
                Cell cell = row.createCell(colNumber++);
                cell.setCellValue(rowData.get(header));
            }
        }

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(filePath);
            workbook.write(fileOutputStream);
            workbook.close();
        } catch (IOException e) {
            throw new FileWriterException(e.getMessage(), e);
        }
    }
}
