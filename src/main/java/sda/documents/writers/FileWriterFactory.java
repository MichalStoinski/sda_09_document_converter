package sda.documents.writers;

import sda.documents.common.AbstractFileReaderWriterFactory;
import sda.documents.common.SupportedFileExtensions;
import sda.documents.exceptions.FileWriterException;

public class FileWriterFactory extends AbstractFileReaderWriterFactory {

    @Override
    public IFileWriter produce(String filePath) throws FileWriterException {
        String extension = getExtension(filePath);
        IFileWriter writer;

        switch (extension) {
            case SupportedFileExtensions.CSV:
                writer = new CsvWriter();
                break;
            case SupportedFileExtensions.XML:
                writer = new XmlWriter();
                break;
            case SupportedFileExtensions.JSON:
                writer = new JsonWriter();
                break;
            case SupportedFileExtensions.EXCEL:
                writer = new ExcelWriter();
                break;
            default:
                throw new FileWriterException(UNKNOWN_FILE_FORMAT + " to write: " + extension);
        }
        return writer;
    }
}

