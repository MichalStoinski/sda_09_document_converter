package sda.documents.writers;

import sda.documents.common.IFileReaderWriter;
import sda.documents.exceptions.FileWriterException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class IFileWriter implements IFileReaderWriter {
    public abstract void write(String filePath, List<Map<String, String>> data) throws FileWriterException;

    List<String> prepareHeaderRow(List<Map<String, String>> data) {
        ArrayList<String> headers = new ArrayList<>();
        data.forEach(record -> record.keySet().forEach(key -> {
            if (!headers.contains(key)) {
                headers.add(key);
            }
        }));
        return headers;
    }
}
