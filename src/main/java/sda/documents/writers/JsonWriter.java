package sda.documents.writers;

import org.json.JSONArray;
import sda.documents.exceptions.FileWriterException;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class JsonWriter extends IFileWriter {
    @Override
    public void write(String filePath, List<Map<String, String>> data) throws FileWriterException {
        JSONArray jsonArray = new JSONArray();
        data.forEach(jsonArray::put);

        FileWriter fileWriter;
        try {
            fileWriter = new FileWriter(filePath);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(jsonArray.toString());
            bufferedWriter.close();
            fileWriter.close();
        } catch (IOException e) {
            throw new FileWriterException(e.getMessage(), e);
        }
    }
}
