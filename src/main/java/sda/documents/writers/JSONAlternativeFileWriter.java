package sda.documents.writers;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.type.CollectionType;
import sda.documents.exceptions.FileWriterException;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class JSONAlternativeFileWriter extends IFileWriter {
    @Override
    public void write(String filePath, List<Map<String, String>> data) throws FileWriterException {
        try {
            FileWriter fileWriter = new FileWriter(filePath);
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            ObjectMapper objectMapper = new ObjectMapper();
            CollectionType collectionType = objectMapper.getTypeFactory().constructCollectionType(List.class, Map.class);
            ObjectWriter objectWriter = objectMapper.writerWithType(collectionType);
            String jasonToSave = objectWriter.writeValueAsString(data);
            bufferedWriter.write(jasonToSave);
            bufferedWriter.close();
            fileWriter.close();
        } catch (IOException e) {
            throw new FileWriterException(e.getMessage(), e);
        }
    }
}
