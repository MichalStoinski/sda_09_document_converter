package sda.documents.writers;

import sda.documents.exceptions.FileWriterException;
import sda.documents.exceptions.NotNormalizedData;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

public class CsvWriter extends IFileWriter {

    private static final String CSV_SEPARATOR = ",";
    private static final String NEW_LINE = "\r\n";
    private static final String QUOTATION_MARK = "\"";

    @Override
    public void write(String filePath, List<Map<String, String>> data) throws FileWriterException {

        try {
            List<String> headers = prepareHeaderRow(data);

            StringBuilder buildLine = new StringBuilder();
            for (String header : headers) {
                buildLine.append(header).append(CSV_SEPARATOR);
            }
            buildLine.deleteCharAt(buildLine.length() - 1);
            buildLine.append(NEW_LINE);

            for (Map<String, String> record : data) {
                if (record.size() > headers.size()) {
                    String header = findMissingHeader(headers, record);
                    throw new NotNormalizedData("Missing data for: " + header);
                }
                for (String header : headers) {
                    String item = record.get(header);
                    if (item == null) {
                        throw new NotNormalizedData("Missing data for: " + header);
                    }
                    if (item.contains(CSV_SEPARATOR)) {
                        item = QUOTATION_MARK + item + QUOTATION_MARK;
                    }
                    buildLine.append(item).append(CSV_SEPARATOR);
                }
                buildLine.deleteCharAt(buildLine.length() - 1);
                buildLine.append(NEW_LINE);
            }

            File file = new File(filePath);
            FileOutputStream outputStream = new FileOutputStream(file);
            OutputStreamWriter osw = new OutputStreamWriter(outputStream, StandardCharsets.UTF_8);
            BufferedWriter writer = new BufferedWriter(osw);

            if (!file.exists()) {
                file.createNewFile();
            }
            writer.append(buildLine); //can also use outputStream instead of writer
            writer.flush();
            writer.close();
        } catch (IOException | NotNormalizedData e) {
            throw new FileWriterException(e.getMessage(), e);
        }

    }

    private String findMissingHeader(List<String> headers, Map<String, String> record) {
        for (String key : record.keySet()) {
            if (!headers.contains(key)) {
                return key;
            }
        }
        return null;
    }
}
