package sda.documents.exceptions;

public class FileWriterException extends IFileReaderWriterException {
    public FileWriterException(String message) {
        super(message);
    }

    public FileWriterException(String message, Throwable cause) {
        super(message, cause);
    }
}
