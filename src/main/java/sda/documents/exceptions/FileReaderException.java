package sda.documents.exceptions;

public class FileReaderException extends IFileReaderWriterException {
    public FileReaderException(String message) {
        super(message);
    }

    public FileReaderException(String message, Throwable cause) {
        super(message, cause);
    }
}
