package sda.documents.exceptions;

public abstract class IFileReaderWriterException extends Exception {
    IFileReaderWriterException(String message) {
        super(message);
    }

    IFileReaderWriterException(String message, Throwable cause) {
        super(message, cause);
    }
}
