package sda.documents.exceptions;

public class NotNormalizedData extends Exception {
    public NotNormalizedData(String message) {
        super(message);
    }
}
