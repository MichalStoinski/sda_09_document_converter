package sda.documents;

import sda.documents.common.AbstractFileReaderWriterFactory;
import sda.documents.exceptions.IFileReaderWriterException;
import sda.documents.readers.FileReaderFactory;
import sda.documents.readers.IFileReader;
import sda.documents.writers.FileWriterFactory;
import sda.documents.writers.IFileWriter;

import java.util.List;
import java.util.Map;

class DocumentConverter {
    private DocumentConverter() {
    }

    static void convert(String inputFilePath, String outputFilePath) throws IFileReaderWriterException {
        //read file
        AbstractFileReaderWriterFactory fileReaderFactory = new FileReaderFactory();
        IFileReader reader = (IFileReader) fileReaderFactory.produce(inputFilePath);
        List<Map<String, String>> data = reader.read(inputFilePath);
        //write file
        AbstractFileReaderWriterFactory fileWriterFactory = new FileWriterFactory();
        IFileWriter writer = (IFileWriter) fileWriterFactory.produce(outputFilePath);
        writer.write(outputFilePath, data);
    }
}
