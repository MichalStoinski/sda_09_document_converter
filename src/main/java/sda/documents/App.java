package sda.documents;

import sda.documents.exceptions.IFileReaderWriterException;

public class App {

    public static void main(String[] args) {
        String inputFilePath = "C:\\Users\\michal.stoinski\\IdeaProjects\\sda_09_document_converter\\" +
                "src\\main\\resources\\input.csv";
        String outputFilePath = "C:\\Users\\michal.stoinski\\IdeaProjects\\sda_09_document_converter\\" +
                "src\\main\\resources\\output.csv";

        try {
            DocumentConverter.convert(inputFilePath, outputFilePath);
        } catch (IFileReaderWriterException e) {
            System.out.println(e.getClass().getSimpleName() + ": " + e.getCause().getClass().getSimpleName() + ": " + e.getCause().getMessage());
        }
    }
}
