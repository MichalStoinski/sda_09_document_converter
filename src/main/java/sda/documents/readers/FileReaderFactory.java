package sda.documents.readers;

import sda.documents.common.AbstractFileReaderWriterFactory;
import sda.documents.common.SupportedFileExtensions;
import sda.documents.exceptions.FileReaderException;

public class FileReaderFactory extends AbstractFileReaderWriterFactory {

    @Override
    public IFileReader produce(String filePath) throws FileReaderException {
        String extension = getExtension(filePath);
        IFileReader reader;

        switch (extension) {
            case SupportedFileExtensions.CSV:
                reader = new CsvReader();
                break;
            case SupportedFileExtensions.XML:
                reader = new XmlReader();
                break;
            case SupportedFileExtensions.JSON:
                reader = new JsonReader();
                break;
            case SupportedFileExtensions.EXCEL:
                reader = new ExcelReader();
                break;
            default:
                throw new FileReaderException(UNKNOWN_FILE_FORMAT + " to read: " + extension);
        }
        return reader;
    }
}
