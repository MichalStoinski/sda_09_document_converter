package sda.documents.readers;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import sda.documents.exceptions.FileReaderException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

public class ExcelReader implements IFileReader {

    private static final int FIRST_SHEET = 0;

    @Override
    public List<Map<String, String>> read(String filePath) throws FileReaderException {
        ArrayList<Map<String, String>> result = new ArrayList<>();
        File file = new File(filePath);
        try {
            FileInputStream excelFile = new FileInputStream(file);
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet sheet = workbook.getSheetAt(FIRST_SHEET);
            Iterator<Row> iterator = sheet.iterator();
            Row row = iterator.next();
            List<String> headers = getHeadersFromFile(row);

            while (iterator.hasNext()) {
                row = iterator.next();
                Iterator<Cell> cells = row.iterator();
                Map<String, String> rowData = new HashMap<>();
                for (String header : headers) {
                    rowData.put(header, cells.next().toString());
                }
                result.add(rowData);
            }
        } catch (IOException e) {
            throw new FileReaderException(e.getMessage(), e);
        }
        return result;
    }

    private List<String> getHeadersFromFile(Row row) {
        ArrayList<String> headers = new ArrayList<>();
        for (Cell cell : row) {
            headers.add(cell.toString());
        }
        return headers;
    }
}
