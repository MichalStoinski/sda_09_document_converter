package sda.documents.readers;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import sda.documents.exceptions.FileReaderException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class XmlReader implements IFileReader {
    @Override
    public List<Map<String, String>> read(String filePath) throws FileReaderException {
        List<Map<String, String>> result;
        File file = new File(filePath);
        try {
            Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(file);
            Element rootElement = document.getDocumentElement();
            rootElement.normalize();
            NodeList elements = rootElement.getChildNodes();
            result = IntStream.range(0, elements.getLength())
                    .mapToObj(elements::item)
                    .filter(node -> node.getNodeType() == Node.ELEMENT_NODE)
                    .map(node -> (Element) node)
                    .map(Node::getChildNodes)
                    .map(dataRawXml -> IntStream.range(0, dataRawXml.getLength())
                            .mapToObj(dataRawXml::item)
                            .filter(subNode -> subNode.getNodeType() == Node.ELEMENT_NODE)
                            .map(subNode -> (Element) subNode)
                            .collect(Collectors.toMap(Element::getTagName, dataElement -> dataElement.getFirstChild()
                                    .getTextContent(), (a, b) -> b, HashMap::new))).collect(Collectors.toList());

        } catch (SAXException | ParserConfigurationException | IOException e) {
            throw new FileReaderException(e.getMessage(), e);
        }

        return result;
    }
}
