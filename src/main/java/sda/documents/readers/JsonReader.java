package sda.documents.readers;

import org.json.JSONArray;
import org.json.JSONObject;
import sda.documents.exceptions.FileReaderException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

public class JsonReader implements IFileReader {
    @Override
    public List<Map<String, String>> read(String filePath) throws FileReaderException {

        ArrayList<Map<String, String>> result = new ArrayList<>();
        byte[] encoded;
        try {
            encoded = Files.readAllBytes(Paths.get(filePath));
        } catch (IOException e) {
            throw new FileReaderException(e.getMessage(), e);
        }
        String fileContent = new String(encoded, StandardCharsets.UTF_8);
        JSONArray jsonFromFile = new JSONArray(fileContent);
        IntStream.range(0, jsonFromFile.length())
                .mapToObj(i -> new JSONObject(jsonFromFile.get(i).toString()))
                .forEach(jsonObject -> {
                    HashMap<String, String> record = new HashMap<>();
                    for (String key : jsonObject.keySet()) {
                        record.put(key, jsonObject.get(key).toString());
                    }
                    result.add(record);
                });
        return result;
    }
}
