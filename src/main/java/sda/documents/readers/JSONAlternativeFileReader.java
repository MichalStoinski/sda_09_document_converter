package sda.documents.readers;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.type.CollectionType;
import sda.documents.exceptions.FileReaderException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

public class JSONAlternativeFileReader implements IFileReader {
    @Override
    public List<Map<String, String>> read(String filePath) throws FileReaderException {
        try {
            byte[] encoded = Files.readAllBytes(Paths.get(filePath));
            String fileContent = new String(encoded, StandardCharsets.UTF_8);
            ObjectMapper objectMapper = new ObjectMapper();
            CollectionType collectionType = objectMapper.getTypeFactory().constructCollectionType(List.class, Map.class);
            return objectMapper.readValue(fileContent, collectionType);
        } catch (IOException e) {
            throw new FileReaderException(e.getMessage(), e);
        }
    }
}
