package sda.documents.common;

import sda.documents.exceptions.IFileReaderWriterException;

public abstract class AbstractFileReaderWriterFactory {
    private static final String DOT_REGEX = "\\.";
    protected static final String UNKNOWN_FILE_FORMAT = "Unknown file format";

    public abstract IFileReaderWriter produce(String extension) throws IFileReaderWriterException;

    protected static String getExtension(String filePath) {
        String[] split = filePath.split(DOT_REGEX);
        return split[split.length - 1];
    }
}
